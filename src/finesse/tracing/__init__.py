"""The beam tracing library of Finesse.

This sub-package includes the core data structures and algorithms used for structuring
and performing beam traces (:mod:`.tree` and :mod:`.forest`, for developer reference
mostly), and tools for executing propagations of beams outside of the context of a
simulation (:mod:`.tools`).
"""
