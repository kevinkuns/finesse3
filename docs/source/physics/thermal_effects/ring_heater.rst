.. include:: /defs.hrst


Thermal lensing and deformations from ring heaters
**************************************************

Ring heaters are used to introduce a thermal substrate lens
within an optic and also actuate on the surface curvature
of the mirror. Such devices allow us to compensate for
self-heating from laser beam absorption. The equations
implemented in :py:mod:`finesse.thermal.ring_heater` are from :cite:`Ramette:16`
which analytically provides the substrate temperature profile
through a cylindrical optic when heated around its barrel
over a small area.

Steady state substrate temperature
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. jupyter-execute::

    import finesse
    import numpy as np
    import matplotlib.pyplot as plt
    import finesse.thermal.ring_heater as ring_heater
    from finesse.materials import FusedSilica
    finesse.init_plotting()

    a = 0.17 # mirror radius
    b = 56e-3 # LIGO ring heater position
    c = 76e-3 # LIGO ring heater position
    h = 0.2  # mirror thickness
    w = 53e-3 # spot size radius
    r = np.linspace(-a, a, 100) # radial points
    z = np.linspace(-h/2, h/2, 100) # longitudinal points
    material = FusedSilica

    T_rh_per_W = ring_heater.substrate_temperature(r, z, a, b, c, h, material)

    plt.pcolormesh(r, z, T_rh_per_W, rasterized=True, shading='auto')
    plt.colorbar(label='T-$\mathrm{T}_{\mathrm{ext}}$ [K/W]')
    plt.xlabel("Radius [m]")
    plt.ylabel("Depth [m]")

The total substrate thermal lens optical path difference can also be
computed using the :meth:`finesse.thermal.ring_heater.thermal_lens` method, again
returning a distortion per Watt of power ring heater power absorbed
by the mirror.

.. jupyter-execute::

    Z_rh_per_W = ring_heater.thermal_lens(r, a, b, c, h, material)
    plt.plot(r, Z_rh_per_W)
    plt.xlabel("Radius [m]")
    plt.ylabel("OPD [m/W]")

Steady state Thermoelastic deformation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

:todo: Still have to develop this feature
