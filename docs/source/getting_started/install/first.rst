.. include:: ../../defs.hrst
.. _installation-first:

First Install
=============

This section provides information on getting started with your first installation of |Finesse|. Although other installation methods are available, we recommend following this guide to set up your initial environment to avoid known issues.

.. TODO: provide link to known issues

Python Installation
-------------------

We recommend using Miniconda to install the Python ecosystem. We have created Conda packages to automate and easily install |Finesse| in one command.

From the section titles, pick the one that suits your condition.

.. TODO: list options as links

:ref:`python-user-conda-installed`

:ref:`python-user-own-environment`

:ref:`windows-no-conda`

:ref:`macos-no-conda`

:ref:`linux-no-conda`

.. _python-user-conda-installed:

I am a Python user and have Anaconda/Conda installed already (Windows, OSX, or Linux)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you already use Conda on your system, then installation is very easy! You can proceed to :ref:`installing-with-conda`.

.. _python-user-own-environment:

I am a Python user and have my own environment already set up that I want to use
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you already have Python installed on your machine and you are using it regularly for other tasks, we still recommend you follow the instructions below to set up a new environment to avoid potential issues. Although installing |Finesse| within an existing environment is possible, the user takes the responsibility to sort out any conflicts that may occur. Please proceed to :ref:`installing-with-conda`.

.. _windows-no-conda:

For Windows users without Conda installed
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#. Download the Python 3.10 Miniconda installer from `the Miniconda website <https://docs.conda.io/en/latest/miniconda.html#windows-installers>`_.
#. Open the executable and follow the instructions on the screen. You can just use all default settings given by the installer.
#. Once Miniconda has been installed, search for "Anaconda Prompt" in the start menu (press the Windows key and type "Anaconda Prompt"). This will open a new command window which can run Conda and Python.
#. Type :code:`conda info` and press enter. If the installation has been successful, you should see the information related to Conda on the screen.

.. (TODO: screen of output)

Now move on to :ref:`installing-with-conda`.

.. _macos-no-conda:

For MacOS users without Conda installed
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#. Download and run the appropriate Python 3.10 Miniconda3 installer for your machine `the Miniconda website <https://docs.conda.io/en/latest/miniconda.html#macos-installers>`_.
#. Type :code:`conda info` and you should see the information related to Conda on the screen.

.. (TODO: screen)

For additional instructions, `see here <https://conda.io/projects/conda/en/stable/user-guide/install/macos.html>`_.

The main Conda environment will be activated each time you open a new terminal and will be ready for you to use straight away. You can tell that the environment is activated by the fact that :code:`(base)` should now appear before each line in the terminal. If you want to switch off the environment, simply type :code:`conda deactivate`.

Now move on to :ref:`installing-with-conda`.

.. _linux-no-conda:

For Linux users without Conda installed
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#. Download and run the appropriate Python 3.10 Miniconda3 installer for your machine `the Miniconda website <https://docs.conda.io/en/latest/miniconda.html#linux-installers>`_.
#. Type :code:`conda info` and you should see the information related to Conda on the screen.

For additional instructions, `see here <https://conda.io/projects/conda/en/stable/user-guide/install/linux.html>`_.

.. (TODO: screen)

The main Conda environment will be activated each time you open a new terminal and will be ready for you to use straight away. You can tell that the environment is activated by the fact that :code:`(base)` should now appear before each line in the terminal. If you want to switch off the environment, simply type :code:`conda deactivate`.

.. _installing-with-conda:

Installing Finesse and Jupyter with Conda
-----------------------------------------

Open up a new Terminal window (or Anaconda Prompt for Windows) and use the following commands one after another:

Make sure Conda itself is up-to-date::

    conda update -n base conda

Note: If your conda installation in sufficiently old, it may need to be reinstalled.

Create and activate a new environment for |Finesse|::

    conda create -n finesse python=3.10
    conda activate finesse

Install the |Finesse| and Jupyter packages from conda-forge::

    conda install -c conda-forge finesse jupyter

Using Jupyter notebook to run Finesse
-------------------------------------

Using Jupyter notebook is a nice way to keep your technical notes with results. The Jupyter notebook should already have been installed on your computer in the last section.

To open Jupyter notebook, open a Terminal window (or Anaconda Prompt for Windows users), and type :code:`jupyter notebook`. Windows users can also find it by searching for it in the start menu (press the Windows key and type "Jupyter Notebook").

The Jupyter notebook will be opened with your default browser. You can go to, or create, your preferred working folder. Press New and choose Python 3 to create a new notebook.

.. (TODO: screen)
