.. include:: ../../../defs.hrst

Structure
~~~~~~~~~

All Cython extensions in |Finesse| must be structured in the same way as the Python
sub-modules. This means that if your extension requires a definition file (`.pxd`) then
it should exist in its own directory / sub-package within the code, where this directory
then contains a `__init__.pxd` file if other Cython extensions need to interact with it
and a `__init__.py` file if the Python code requires access to the extension.

Additionally, similarly to the Python code, if your Cython extension is comprised of
multiple `.pyx` files then it should also exist in a separate directory (even if no
`.pxd` files are needed).

For a brief summary on why it is important to follow the same structure as the Python
package itself, see this `FAQ answer
<https://github.com/cython/cython/wiki/FAQ#how-to-compile-cython-with-subpackages>`_ on
the Cython GitHub Wiki pages.
